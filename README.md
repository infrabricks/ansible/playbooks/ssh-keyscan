# SSH keyscan

Ansible playbook to populate known hosts file. 

## OS

* Debian

## Requirements

* Ansible >= 4.

## Role Variables

Variable name      | Description                              | Required | Default
---                | ---                                      | ---      | ---
inventory_hostname | Hostname to register in known hosts file | yes      | yes

## Dependencies

No.

## License

* GPL v3

## Author Information

* [Stéphane Paillet](mailto:spaillet@ethicsys.fr)

